/*
 * ------------------------------------------------------------------------
 * MGILLIAM ADDITIONS
 * ------------------------------------------------------------------------
 */

//Sliders --DEFAULT SYSTEM, I THINK  --------------------------------------

//var slider = document.getElementById("sleepHours");
//var output = document.getElementById("dynamicSet");
//var isShow = true
//output.innerHTML = slider.value
//update=()=>{

//output.innerHTML = slider.value;
// Display the default slider value
//console.log(slider.value)
//}

// Update the current slider value (each time you drag the slider handle)
//let update = () => output.innerHTML = slider.value;

//slider.addEventListener('input', update);

//Color-Theme toggle--------------------------------------------------------
// function to set a given theme/color-scheme
function setTheme(themeName) {
    localStorage.setItem('theme', themeName);
    document.documentElement.className = themeName;
}
// function to toggle between light and dark theme
function toggleTheme() {
   if (localStorage.getItem('theme') === 'theme-Main'){
       setTheme('theme-ColorBlind');
   } else {
       setTheme('theme-Main');
   }
}
// Immediately invoked function to set the theme on initial load
(function () {
   if (localStorage.getItem('theme') === 'theme-Main') {
       setTheme('theme-Main');
   } else {
       setTheme('theme-ColorBlind');
   }
})();

//functions to set Themes - no toggler
function setMain() {
    setTheme('theme-Main');
}
function setColorBlind() {
    setTheme('theme-ColorBlind');
}

//Username Variable into localStorage ---------------------------------------
var usersName = "anonymous"
function storeUsersName(){
   var usersName= document.getElementById("inputUserName");
   localStorage.setItem("usersName", inputUserName.value);
 };
 function stayAnon(){
    var usersName= document.getElementById("inputUserName");
    localStorage.setItem("usersName", "Anonymous");
  };

// PLACEHOLDER functions-----------------------------------------------------
function saveQuestions(){
 };
function triggerBluetooth(){
};
function triggerTouchID(){
};
function triggerPasscode(){
};
